### Little Brown Bat

#### Introduction
Little brown bats were once very common in north america,
but due to the destruction of nesting sites and hibernaculas they are now
endangered, with populations declining as much as 99% in some areas. 

Best ways of helping them are by setting up bat houses for them to breed, and
bat hibernacula for them to have a place to overwinter. 

#### Bat houses (nestbox)

Bat houses are often made of plywood, but due to urine it tends to expand and is
no longer usable after a few years. Corrugated plastic is a safe alternative
which is non-toxic and resistant to acidic bat urine, so makes a better cling
surface for the bats.  The corrugated plastic is recycled from political signs. 

The styrofoam is recycled from packaging, and provides good insulation helping
the little brown bats stay warm in their nestbox. 

The brown aluminum flashing keeps the rain out and keeps the bats dry, is quite
economical, and aluminum is easy to recycle. 

#### files

There are several png and jpg files where you can view the bathouse,
and the fcstd file can be run in freecad to evaluate it better. 

#### Links

https://www.cwf-fcf.org/en/resources/encyclopedias/fauna/mammals/little-brown-bat.html
